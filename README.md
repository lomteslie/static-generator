# Fuzz Static Generator

> Yeoman generator for Fuzz Web app static directories.

## Usage
Install `generator-fuzz`:

	npm install git+ssh://git@gitlab.fuzzhq.com:web-modules/static-generator.git#master -g

Create project directory

	mkdir <project-directory>
	cd <project-directory>

Generate!

	yo fuzz


## Working with Gulp
`cd static` and run `gulp build [--prod]` to produce builds.

### Adding dependencies
If you need to add a dependency, first install it with bower and note the destination of the files you need in the lib directory.

Next, create a new json file in the gulp/config/libs directory like the example shown below, and add your library paths.

**example:** *gulp/config/libs/angular.json*

	{
		"js": [
			"lib/angular/angular.js"
		],
		"css": [],
		"img": [],
		"fonts": []
	}

Now drop the name of your new dependency config file in your libraries object:

**example:** *gulp/config/index.js*

	var libraries = {
		main: ['angular']
	};

Each `libraries` key corresponds to a file that will become concatendated and minified within your build/libs directory. The keys are set to `main` and `legacy` by default, but they can be changed to whatever you want.

## Sass Extensions
Adding sass extensions works similarly to loading libraries. First, load any extensions through bower. Then specify them by name in the configurable array inside gulp/config/sass.js. The names inside the array should match the directory names inside the libs directory. Pelorus comes loaded by default.

## Known Issues
- There are no tests yet.
- More common dependencies need to be added, including the following:

	- angular-file-upload
	- jquery
	- modernizr
	- json3
	- enquire
	- angular-touch
	- fzFormUI
	- placeholder