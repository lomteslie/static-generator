'use strict';
/**
 * Fuzz Static Generator
 *
 * @module  fuzzStaticGenerator
 * @author  Tom Leslie <tom@fuzzproductions.com>
 * @version  1.0.0
 * @description  Generate a static directory with the the fixins
 */
var yeoman = require('yeoman-generator');

/**
 * Static directory
 *
 * @const {string}
 */
var STATIC_DIR = 'static/';

var FuzzGenerator = yeoman.generators.Base.extend({

	// Override destination root
	initializing: function() {
		this.destinationRoot(STATIC_DIR);
	},

	// Set up our basic directory scaffolding
	default: function() {
		this.mkdir('build');
		this.mkdir('build/js');
		this.mkdir('build/css');
		this.mkdir('build/img');
		this.mkdir('build/img/sprite');
		this.mkdir('build/img/sprite-retina');
		this.mkdir('build/fonts');
		this.mkdir('build/libs');
		this.mkdir('build/libs/js');
		this.mkdir('build/libs/css');
		this.mkdir('build/libs/img');
		this.mkdir('build/libs/fonts');
		this.mkdir('gulp');
		this.mkdir('gulp/tasks');
		this.mkdir('gulp/util');
		this.mkdir('gulp/config');
		this.mkdir('gulp/config/libs');
		this.mkdir('js');
		this.mkdir('lib');
		this.mkdir('sass');
		this.mkdir('sass_extensions');
		this.mkdir('test');
	},

	// Copy our template files
	writing: function() {
		this.src.copy('_gitignore', '.gitignore');
		this.src.copy('.scss-lint.yml', '.scss-lint.yml');
		this.src.copy('.bowerrc', '.bowerrc');
		this.src.copy('bower.json', 'bower.json');
		this.src.copy('package.json', 'package.json');
		this.src.copy('eslint.json', '.eslintrc');
		this.src.copy('gulpfile.js', 'gulpfile.js');
		this.src.copy('config.rb', 'config.rb');
		this.src.copy('compass-sprite.png', 'build/img/sprite/REMOVE_ME.png');
		this.src.copy('compass-sprite.png', 'build/img/sprite-retina/REMOVE_ME.png');
		this.directory('gulp/config', 'gulp/config');
		this.directory('gulp/tasks', 'gulp/tasks');
		this.directory('gulp/util', 'gulp/util');
		this.directory('js', 'js');
		this.directory('sass', 'sass');
		this.directory('lint_rules', 'lint_rules');
	},

	// Run bower and NPM
	install: function() {
		this.installDependencies();
	}
});

module.exports = FuzzGenerator;
