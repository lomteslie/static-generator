'use strict';

/**
 * Fuzz gulp configuration
 * @param {boolean} prod Whether we're in production mode or not
 * @returns {object} Gulp config
 */
module.exports = function(prod) {
	/**
	 * Library dependency loading
	 * Add all library dependencies here for automagic globulation.
	 * @type {object}
	 * @example
	 * 		{main: ['angular', 'angular-route']}
	 * 		This will look for an angular.json and an angular-route.json,
	 * 		and will result in a single main.js file in the build directory.
	 */
	var libraries = {
		main: ['angular'],
		legacy: ['html5shiv']
	};

	/**
	 * Scripts config
	 * @type {object}
	 */
	var scripts = require('./scripts.js')(prod);

	/**
	 * Sass config
	 * @type {object}
	 */
	var sass = require('./sass.js')(prod);

	/**
	 * Paths config
	 * @type {object}
	 */
	var paths = require('./paths.js')(libraries);

	return {
		prod: prod,
		js: scripts,
		sass: sass,
		paths: paths
	};
};
