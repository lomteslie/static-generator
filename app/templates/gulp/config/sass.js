'use strict';
/**
 * Sass configuration
 * @param {boolean} prod Whether we're in production mode or not
 * @returns {object} Sass config
 */
module.exports = function(prod) {
	return {
		style: prod ? 'compressed' : 'expanded',
		comments: !prod,
		extensions: ['pelorus']
	};
};
