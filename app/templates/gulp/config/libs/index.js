'use strict';
/**
 * Library config relay
 *
 * @module	fuzzGulp/config/libs
 * @description
 * 	This directory should only contain this file and json config files that
 * 	correspond to currently installed bower dependencies.
 *
 * 	Instead of manually having to associate each config file with an object property of this module,
 * 	we utilize the require-directory package to automatically make those associations.
 *
 * 	When adding a library config file, make sure to add scripts, styles, images, and fonts in separate arrays.
 * 	The paths config will take care of putting them where they belong.
 */
var requireDirectory = require('require-directory');

module.exports = requireDirectory(module);
