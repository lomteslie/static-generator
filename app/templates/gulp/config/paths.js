'use strict';

/**
 * Paths configuration
 * @module	fuzzGulp/config/paths
 * @description	Specifies the src and dest paths for scripts, sass, and libraries.
 */
var log = require('../util/logger');

module.exports = function(chosenLibs) {
	var libConfig = require('./libs');
	var fileLibs  = {
		js: [],
		css: []
	};
	var libraries = {
		js: [],
		css: [],
		img: [],
		fonts: []
	};
	// For loop convenience variables
	var index, type, libIndex, list, name, config;

	// Populate our new libraries object with the chosen library values
	for (index in chosenLibs) {
		// Create a new libraries object key that corresponds to the chosenLibs values
		libraries.js[index]  = [];
		libraries.css[index] = [];

		// Construct our files objects for scripts and styles
		fileLibs.js[index]  = index + '.js';
		fileLibs.css[index] = index + '.css';
	}

	/**
	 * Loop through our chosen libraries and check them against the library configs.
	 * Sort them by type and save them to the libs src and file keys.
	 */
	for (type in chosenLibs) {
		list = chosenLibs[type];

		for (libIndex in list) {
			name = list[libIndex];

			if (libConfig.hasOwnProperty(name)) {
				config = libConfig[name];

				// Map our scripts
				libraries.js[type] = libraries.js[type].concat(config.js);

				// Map our styles
				libraries.css[type] = libraries.css[type].concat(config.css);

				// Map our images
				libraries.img = libraries.img.concat(config.img);

				// Map our fonts
				libraries.fonts = libraries.fonts.concat(config.fonts);
			} else {
				log.error(name + ' was listed as a dependency but no matching config file was found. Either remove the dependency or add a corresponding config file.');
			}
		}
	}

	return {
		src: {
			js: [
				'gulp/config/templates/header',
				'js/**/*.js',
				'gulp/config/templates/footer'
			],
			sass: 'sass/**/*.scss',
			libs: libraries
		},
		dest: {
			js: 'build/js',
			sass: 'build/css',
			sassExt: 'sass_extensions',
			libs: {
				js: 'build/libs/js',
				css: 'build/libs/css',
				img: 'build/libs/img',
				fonts: 'build/libs/fonts'
			}
		},
		files: {
			js: 'app.js',
			libs: fileLibs
		},
		tmp: '.tmp'
	};
};
