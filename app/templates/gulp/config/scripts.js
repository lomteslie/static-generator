'use strict';

/**
 * Javascript configuration
 * @param {boolean} prod Whether we're in production mode or not
 * @returns {object} Scripts config
 */
module.exports = function(prod) {
	return {
		comments: prod ? false : 'all',
		compress: prod ? {} : false,
		mangle: prod
	};
};
