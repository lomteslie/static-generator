'use strict';

/**
 * Utility manager
 * @exports {object} logger
 * @description Single point for loading all custom utilities
 */
exports.logger = require('./logger');
