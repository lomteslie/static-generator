'use strict';

var chalk = require('chalk');

/**
 * Logger
 * @description Beautiful console logging
 * @exports {object} Differently colored conslole logs based on severity
 */
module.exports = {
	success: function(string) {
		console.log(chalk.green(string));
	},
	warning: function(string) {
		console.log(chalk.yellow(string));
	},
	error: function(string) {
		console.log(chalk.red(string));
	},
	info: function(string) {
		console.log(chalk.cyan(string));
	},
	styleWarning: function(line, reason) {
		console.log(chalk.cyan(line), chalk.yellow(reason));
	},
	styleError: function(line, reason) {
		console.log(chalk.red.bgWhite(line), chalk.yellow(reason));
	}
};
