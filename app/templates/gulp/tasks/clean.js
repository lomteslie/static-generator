'use strict';

/**
 * Delete
 */
var del = require('del');

/**
 * Cleaning tasks
 * @description Clean up our gulp mess
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @returns {void}
 */
module.exports = function(gulp, config) {
	/**
	 * Task for cleaning up the tmp directory
	 * @returns {void}
	 */
	gulp.task('clean:tmp', function() {
		del(config.paths.tmp);
	});

	/**
	 * Task for cleaning up the build directory
	 * @returns {void}
	 */
	gulp.task('clean:build', function() {
		del(config.paths.dest.js + '/*.js');
		del(config.paths.dest.sass + '/*.css');
		del(config.paths.dest.libs.js + '/*.js');
		del(config.paths.dest.libs.css + '/*.css');
		del(config.paths.dest.libs.img + '/*');
		del(config.paths.dest.libs.fonts + '/*');
	});
};
