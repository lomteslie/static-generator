'use strict';
/**
 * Task loader
 *
 * @module  fuzzGulp/tasks
 * @description  Single point for loading all custom gulp tasks.
 */

var runSequence = require('run-sequence');

module.exports = function(gulp, config, prod) {
	// Include all our tasks so we can call them below
	require('./concat')(gulp, config);
	require('./compress')(gulp, config);
	require('./lint')(gulp, config, prod);
	require('./compass')(gulp, config);
	require('./clean')(gulp, config);
	require('./copy-assets')(gulp, config);

	return {
		runSequence: function(sequence) {
			switch (sequence) {
				case 'prod': // Production tasks
					return runSequence(
						'clean:build',
						'copy-assets:sass-extensions',
						'compass',
						'lint:js',
						'lint:scss',
						'concat:src',
						'compress:src',
						'copy-assets:libs',
						'concat:libs',
						'compress:libs',
						'clean:tmp'
					);
					break;
				case 'lib': // Library tasks
					return runSequence(
						'clean:build',
						'copy-assets:sass-extensions',
						'compass',
						'concat:src',
						'compress:src',
						'copy-assets:libs',
						'concat:libs',
						'compress:libs',
						'clean:tmp'
					);
					break;
				case 'watch:js': // Script tasks to watch
					return runSequence(
						'lint:js',
						'concat:src',
						'compress:src',
						'clean:tmp'
					);
					break;
				case 'watch:sass': // Sass tasks to watch
					return runSequence('compass', 'lint:scss');
					break;
				default:
					return false;
					break;
			}
		}
	};
};
