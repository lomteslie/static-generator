'use strict';

/**
 * Event Stream
 * @type {exports}
 */
var es = require('event-stream');

/**
 * File System
 * @type {object}
 */
var fs = require('fs');

/**
 * Gulp Uglify
 * @type {exports}
 */
var uglify = require('gulp-uglify');

/**
 * Gulp CSS Min
 * @type {exports}
 */
var cssmin = require('gulp-cssmin');

/**
 * Compression/uglification tasks
 * @description Take a concatenated script file, compress it, and pipe it into it's final destination.
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @returns {void}
 */
module.exports = function(gulp, config) {
	var paths = config.paths;

	/**
	 * Compress scripts
	 * @description
	 *		This task currently takes the concatenated script file and places it into the dest folder
	 *		without any compression. This also keeps our sourcemaps in tact.
	 * @returns {object}
	 * 		Gulp stream
	 */
	gulp.task('compress:src', function() {
		var stream = gulp.src(paths.tmp + '/' + paths.files.js);

		if (config.prod) {
			stream = stream.pipe(uglify({
				compress: true,
				mangle: true
			}));
		}

		return stream.pipe(gulp.dest(paths.dest.js));
	});

	/**
	 * Compress libs
	 * @returns {object} Gulp stream
	 */
	gulp.task('compress:libs', function() {
		var streams = [];
		var filePath, jsLib, cssLib;

		// Compress scripts
		for (jsLib in paths.files.libs.js) {
			filePath = paths.tmp + '/' + paths.files.libs.js[jsLib];

			if (fs.existsSync(filePath) === true) {
				streams = streams.concat(gulp.src(filePath).pipe(uglify({
					compress: true,
					mangle: true
				})).pipe(gulp.dest(paths.dest.libs.js)));
			}
		}


		// Compress styles
		for (cssLib in paths.files.libs.css) {
			filePath = paths.tmp + '/' + paths.files.libs.css[cssLib];

			if (fs.existsSync(filePath) === true) {
				streams = streams.concat(gulp.src(filePath)
					.pipe(cssmin())
					.pipe(gulp.dest(paths.dest.libs.css)));
			}
		}

		return es.concat.apply(null, streams);
	});
};
