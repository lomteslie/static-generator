'use strict';

/**
 * Gulp If
 * @type {exports}
 */
var gulpif = require('gulp-if');

/**
 * Gulp ESLint
 * @type {exports}
 */
var eslint = require('gulp-eslint');

/**
 * Load our logger
 * @type {object}
 */
var logger = require('../util').logger;

/**
 * Gulp SCSS Lint
 * @type {exports}
 */
var scsslint = require('gulp-scss-lint');

/**
 * SCSS Reporter
 * @param  {object} file Gulp stream with scss linter properties.
 * @return {void}
 */
var scssReporter = function(file) {
	// Check for errors
  if (! file.scsslint.success) {
  	var errorIndex, error, line;

  	// Log errors in file
    logger.info(file.scsslint.issues.length + ' issues found in ' + file.path);

    for (errorIndex in file.scsslint.issues) {
    	error = file.scsslint.issues[errorIndex];
    	line  = '[' + error.line + ']';

    	if (error.severity === 'warning') {
    		// log a warning
    		logger.styleWarning(line, error.reason);
    	} else {
    		// log an error
    		logger.styleError(line, error.reason);
    	}
    }
  }
};

/**
 * Linting tasks
 * @description Lint scripts utilizing eslint
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @param {boolean} prod Whether we're in production mode or not
 * @returns {void}
 */
module.exports = function(gulp, config, prod) {
	/**
	 * Primary lint task
	 * @returns {object} Gulp stream
	 */
	gulp.task('lint:js', function() {
		return gulp.src(config.paths.src.js)
			.pipe(eslint({
				rulesdir: 'lint_rules/',
				globals: {
					'angular': false,
					'jQuery': false
				},
				env: {
					browser: true
				}
			}))
			.pipe(eslint.format());
	});

	/**
	 * SCSS lint task
	 * @returns {object} Gulp stream
	 */
	gulp.task('lint:scss', function() {
		return gulp.src(config.paths.src.sass)
			.pipe(scsslint({
				config: '.scss-lint.yml',
				customReport: scssReporter,
			}));
	});
};
