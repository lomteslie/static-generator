'use strict';

/**
 * Gulp Compass
 * @type {exports}
 */
var compass = require('gulp-compass');

/**
 * Tasks for managing compass and sass operations
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @returns {void}
 */
module.exports = function(gulp, config) {
	/**
	 * Compass gulp task
	 * @returns {object} Gulp stream
	 */
	gulp.task('compass', function() {
		return gulp.src(config.paths.src.sass)
			.pipe(compass({
				css: config.paths.dest.sass,
				style: config.sass.style
			}));
	});
};
