'use strict';

/**
 * Event Stream
 * @type {exports}
 */
var es = require('event-stream');

/**
 * Gulp Concat
 * @type {exports}
 */
var concat = require('gulp-concat');

/**
 * Gulp Sourcemaps
 * @type {exports}
 */
var sourcemaps = require('gulp-sourcemaps');

/**
 * Concat tasks
 * @description  Concatenate scripts together into single files
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @returns {void}
 */
module.exports = function(gulp, config) {
	var paths = config.paths;

	/**
	 * Concat src task
	 * @description Concatenate all our source scripts.
	 * @returns {object} Gulp stream
	 */
	gulp.task('concat:src', function() {
		return gulp.src(paths.src.js)
			.pipe(sourcemaps.init())
			.pipe(concat(paths.files.js))
			.pipe(sourcemaps.write())
			.pipe(gulp.dest(paths.tmp));
	});

	/**
	 * Concat lib task
	 * @description This task will output as many files as are defined within the fuzzGulp/config libraries object.
	 * @returns {object} Gulp Stream
	 */
	gulp.task('concat:libs', function() {
		var streams = [];
		var fileType, files, index, libraries, fileName, libConfig, libIndex, lib;

		// Find our file types and file names to concatenate into
		for (fileType in paths.files.libs) {
			files = paths.files.libs[fileType];

			// Create a library array for every set of libraries to be concatenated
			for (index in files) {
				libraries = [];
				fileName  = files[index];
				libConfig = paths.src.libs[fileType][index];

				for (libIndex in libConfig) {
					lib = libConfig[libIndex];

					libraries.push(lib);
				}

				streams = streams.concat(gulp.src(libraries)
					.pipe(concat(fileName))
					.pipe(gulp.dest(paths.tmp)));
			}
		}

		return es.concat.apply(null, streams);
	});
};
