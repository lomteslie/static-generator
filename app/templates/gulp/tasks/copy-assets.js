'use strict';

/**
 * Event Stream
 * @type {exports}
 */
var es = require('event-stream');

/**
 * Copy tasks
 * @description Move files around
 * @param {object} gulp Gulp instance to attach streams to
 * @param {object} config Global configuration
 * @returns {void}
 */
module.exports = function(gulp, config) {
	var paths = config.paths;

	/**
	 * Copy lib task
	 * @description This is used for copying any lib fonts and images
	 * @returns {object} Gulp stream
	 */
	gulp.task('copy-assets:libs', function() {
		var streams = [];
		var lib, index, assetPath;

		for (lib in paths.src.libs) {
			if (lib === 'img' || lib === 'fonts') {
				for (index in paths.src.libs[lib]) {
					assetPath = paths.src.libs[lib][index];

					streams = streams.concat(
						gulp.src(assetPath).pipe(gulp.dest(paths.dest.libs[lib]))
					);
				}
			}
		}

		// Exit with 0 if there is no stream
		if (! streams.length) {
			return 0;
		}

		return es.concat.apply(null, streams);
	});

	/**
	 * Copy Sass Extensions task
	 * @description Any libraries that are specified in the sass config file will be loaded here.
	 * @returns {object} Gulp stream
	 */
	gulp.task('copy-assets:sass-extensions', function() {
		var streams = [];
		var index, src;

		for (index in config.sass.extensions) {
			src = 'lib/' + config.sass.extensions[index] + '/**';

			streams = streams.concat(
				gulp.src(src, {base: 'lib/'}).pipe(gulp.dest(paths.dest.sassExt))
			);
		}

		// Exit with 0 if there is no stream
		if (! streams.length) {
			return 0;
		}

		return es.concat.apply(null, streams);
	});
};
