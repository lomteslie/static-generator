http_path       = "/"
sass_dir        = "sass"
css_dir         = "build/css"
images_dir      = "build/img"
fonts_dir       = "build/fonts"
javascripts_dir = "js"
extensions_dir  = "sass_extensions"

relative_assets = true
