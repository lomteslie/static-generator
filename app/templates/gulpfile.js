'use strict';

/**
 * Fuzz gulp task runner
 *
 * @module  fuzzGulp
 * @author  Tom Leslie <tom@fuzzproductions.com>
 * @version  0.0.1
 * @description  Run tasks for a fuzz web static directory
 */
var gulp  = require('gulp');
var flags = require('minimist')(process.argv.slice(2));
var prod  = flags.production || flags.prod || false;

/**
 * Load our config object
 * @type {object}
 */
var config = require('./gulp/config')(prod);

/**
 * Load our utlity object
 * @type {object}
 */
var util = require('./gulp/util');

/**
 * Access our asset source paths
 * @type {object}
 */
var srcPath = config.paths.src;

/**
 * Access our logger utility
 * @type {function}
 */
var log = util.logger;

/**
 * Load our tasks
 * @type {object}
 */
var tasks = require('./gulp/tasks')(gulp, config, prod);

/**
 * Primary build task
 * @note Pass the "--prod" flag to activate production environment.
 * @returns {void}
 */
gulp.task('build', function() {
	if (prod) {
		log.info('Running production build.');

		// Run all our tasks
		tasks.runSequence('prod');
	} else {
		log.info('Running development build and watching for changes.');

		// Run our lib tasks
		tasks.runSequence('lib');

		// Run watchers
		gulp.watch(srcPath.js, function() {
			return tasks.runSequence('watch:js');
		});

		gulp.watch(srcPath.sass, function() {
			return tasks.runSequence('watch:sass');
		});
	}
});
